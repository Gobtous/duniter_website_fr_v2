::uml::

@startuml

database "\nBase de donnees Duniter (SQLite)" {

  package "Index Blockchain" {
      component b_index
      component m_index
      component i_index
      component s_index
      component c_index
  }

  package "Piscines" {
      component idty
      component cert
      component membership
      component txs
  }

  package "Autre" {
      component block
      component peer
      component wallet
      component meta
  }
}

@enduml

::end-uml::