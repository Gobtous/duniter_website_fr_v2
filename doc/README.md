# Guide de contribution

Ce guide tout public va vous aider à réaliser une contribution sur le site web, en fonction de la nature de la modification que vous souhaitez apporter.

## Introduction

Le contenu du site est rédigé sous forme de fichiers texte avec une partie "en-tête" contenant des métadonnées au format TOML (`clé = "valeur"` par exemple), et une partie contenu rédigée en Markdown. Certaines pages spécifiques contiennent la métadonnées "template" qui pointe vers une page rédigée en html. Cela permet plus de souplesse dans la rédaction de la page, contre une légère perte en simplicité.

```txt
+++
en-tete
+++

contenu

```

- [en lire plus sur l'en-tête](./en-tete.md)
- [en lire plus sur le contenu](./contenu.md)
- [en lire plus sur les pages custom](./custom.md)

## Corriger une erreur

Pour corriger une erreur comme une faute de frappe, le plus simple est de copier le texte que vous souhaitez modifier et de faire une recherche dans tout le code, par exemple avec la fonctionnalité de recherche dans un éditeur comme vscodium (Ctrl+Maj+F) ou via une commande `grep` récursive :

```bash
grep -r "texte avec erreur" content/
```

Vous saurez ainsi directement quel fichier éditer sans avoir à le chercher.

## Modifier une image

Si vous souhaitez modifier une image, regardez le nom du fichier et faites un recherche, par exemple Ctrl+P sur vscodium, ou en ligne de commande :

```bash
find -name "image.png"
```

## Changer une page custom

Les pages custom sont dans le dossier `templates/custom/`. Pour écrire du css custom, lire [le guide](./custom.md)

## Proposer ses modifications

Veuillez faire une Merge Request en résumant rapidement votre contribution. Si vous ne savez pas comment faire ou que votre modification est mineure, veuillez la signaler sur le forum en notifiant @HugoTrentesaux.

## Publication

TODO détailler CI