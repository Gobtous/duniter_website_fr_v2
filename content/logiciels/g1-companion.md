+++
title = "Ğ1-compagnon"

[taxonomies]
authors = ["manutopik"]
language = ["javascript"]
framework = ["nuxt"]

[extra]
logo = "/img/g1compagnon.svg"
repo = "https://git.duniter.org/clients/g1-compagnon"
+++


Extension de navigateur pour la gestion de clés Ǧ1v2. Elle expose une API permettant à n'importe quel site web de fournir des fonctionnalités liées à la Ǧ1v2.