+++
title = "WotMap"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/wotmap.jpg"
repo = ""
website = "https://wotmap.duniter.org/"

+++

La <a href="https://wotmap.duniter.org/">WotMap</a> est un logiciel de visualisation de la toile de confiance sous forme d'un graphe.