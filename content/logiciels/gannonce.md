+++
title = "Ğannonce"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-bullhorn"
repo = ""
website = ""

+++

<a href="https://gannonce.duniter.org/">Ğannonce</a> est une place de marché permettant d'effectuer des financements participatifs. C'est un plugin pour nœud Duniter qui utilise les commentaires de transaction pour mettre à jour la barre de financement.