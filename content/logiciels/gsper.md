+++
title = "Ğsper"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/gsper-logo.svg"
repo = ""
website = "https://g1.frama.io/gsper/"

+++

<a href="https://g1.frama.io/gsper/">Ğsper</a> permet d'essayer de retrouver un mot de passe perdu par force brute (<a href="https://forum.monnaie-libre.fr/t/rml14-erreur-de-transaction-portefeuille-fantome/8573/34">alt</a>).