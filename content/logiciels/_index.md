+++
title = "Logiciels"
template = "software/all.html"
page_template = "software/single.html"
weight = 3
description = "Un large éventail logiciel constitue l'écosystème de la monnaie libre ǧ1."
aliases = ["ecosysteme"]

[extra]
item_path = "logiciels/"

[[extra.group]]
name = "Écosystème logiciel"
list = ["duniter", "cesium", "silkaj", "gchange", "wotwizard", "datapods", "ginkgo", "g1superbot"]
group_by = 3
[[extra.group]]
name = "En développement"
list = ["duniter-v2", "gecko", "tikka", "g1-companion", ]
group_by = 3
[[extra.group]]
name = "Bibliothèques"
list = ["duniterpy", "g1lib", "durt"]
group_by = 3
[[extra.group]]
name = "De nombreux utilitaires"
list = [
    "gannonce",
    "remuniter",
    "barre-integrable",
    "gsper",
    "dex",
    "gmixer",
    "gcli",
    "dup-tools",
    "vanitygen",
    "wotmap",
    "animwotmap",
    "worldwotmap",
    "g1cotis",
    "g1pourboire",
    "g1sms",
    "g1billet",
    "g1tag",
    "g1lien",
    "g1-monit",
    "gakpot",
    "little-tools",
    "jaklis",
    "bog",
    "g1-stats",
    "dunixir",
    "gexplore",
    "wotwizard-ui",
    "vignette",
    "kazou",
    "ginspecte",
    "GecoHelper",
]
group_by = 4

+++
