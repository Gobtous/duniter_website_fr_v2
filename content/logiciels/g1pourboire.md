+++
title = "Ğ1Pourboire"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-money"
repo = ""
website = "https://g1pourboire.fr/"

+++

<a href="https://g1pourboire.fr/">Ğ1Pourboire</a> permet d'imprimer des des codes d'accès à un portefeuille dédié qui peuvent êtres utilisés pour laisser un pourboire.