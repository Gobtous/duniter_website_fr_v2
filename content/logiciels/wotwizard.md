+++
title = "WotWizard"

[taxonomies]
authors = ["gerard94", ]
language = ["go", ]
framework = []

[extra]
logo = "/img/wizard.svg"
repo = "https://git.duniter.org/gerard94/wotwizard"
+++


Développé en <strong>Go</strong> et exposant une API <strong>GraphQL</strong>, <a href="https://git.duniter.org/gerard94/wotwizard">WotWizard</a> fournit des informations sur l'historique de la toile de confiance ansi que des prédictions sur les entrées.