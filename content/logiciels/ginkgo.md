+++
title = "Ğ1nkgo"

[taxonomies]
authors = ["vjrj", ]
language = ["dart", ]
framework = ["flutter", ]

[extra]
logo = "/img/ginkgo.svg"
repo = "https://git.duniter.org/vjrj/ginkgo/"
website = "https://g1nkgo.comunes.org/"
+++


Application <strong>Flutter</strong> utilisant l'API GVA de Duniter 1.9 permettant de faire des paiement rapides.