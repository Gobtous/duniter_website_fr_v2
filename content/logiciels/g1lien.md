+++
title = "Ǧ1Lien"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-link"
repo = "https://git.duniter.org/clients/g1lien/#%C7%A71lien"
website = ""

+++

<a href="https://git.duniter.org/clients/g1lien/#%C7%A71lien">Ǧ1Lien</a> définit la syntaxe d'url dédiées à la Ǧ1, tel que <a href="g1://pay:100:to:1000i100">g1://pay:100:to:1000i100</a>.