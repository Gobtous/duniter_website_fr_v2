+++
title = "Ğ1Tag"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-qrcode"
repo = ""
website = "https://www.g1sms.fr/fr/blog/g1tag"

+++

<a href="https://www.g1sms.fr/fr/blog/g1tag">Ğ1Tag</a> est une capsule IPFS chiffrée qui conserve le montant en centimes de Ḡ1 (ZEN) dépensés à sa création