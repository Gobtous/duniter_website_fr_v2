+++
title = "Ğ1superbot"

[taxonomies]
authors = ["kapis"]
language = ["python"]
framework = ["django"]

[extra]
logo = "/img/g1superbot.jpg"
repo = "https://git.duniter.org/kapis/g1-wallet-bot"
website = "https://t.me/g1superbot"
+++

Ğ1superbot is a chatbot wallet based on Django and compatible with Matrix and Telegram.