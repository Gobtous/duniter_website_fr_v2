+++
title = "Contributeurs"
date = 2020-07-29
weight = 5
template = "custom/team.html"
page_template = "authors/page.html"

[extra]
# contrôle la liste des contributeurs (nom de fichier)
authors = [
    "cgeek",
    "kimamila",
    "Moul",
    "1000i100",
    "vit",
    "Paidge",
    "gerard94",
    "tuxmain",
    "poka",
    "HugoTrentesaux",
    "manutopik",
    "kapis",
    "vjrj",
    "immae",
    "pini",
    "elois",
    ]
+++

Nous listons ici les personnes ayant apporté une contribution technique au projet. Pour y apparaître, veuillez faire une MR sur [le dépôt](https://git.duniter.org/websites/duniter_website_fr_v2) du site.

Si vous souhaitez contribuer mais ne savez pas comment, veuillez consulter la [page "contribuer"](@/wiki/contribuer/_index.md).