+++
title = "immae"
description = "adminsys/devops de l'infra du projet Duniter"

[extra]
full_name = "Immae"
avatar = "immae.jpg" # image dans /static/equipe
website = "https://www.immae.eu/"
forum_duniter = "immae"
g1_pubkey = "8YJ5aBV8NVnhawt8RTsENhxfb27PQd84NtgJK2sYuqDp"
email = "contact@mail.immae.eu"
gitduniter = "immae"
matrix = "@immae:yuno.librezo.fr"

[taxonomies]
authors = ["immae",]
+++

Je suis un développeur et sysadmin, je travaille avec et pour des outils libres et je m’intéresse un peu aux cryptomonnaies.