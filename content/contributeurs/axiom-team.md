+++
title = "axiom-team"
description = "association pour le développement technique de la Ğ1"

[extra]
full_name = "Axiom Team"
avatar = "axiom-team.png"
website = "https://axiom-team.fr/"
g1_pubkey = "8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8F2zKCthh"
email = "contact@axiom-team.fr"
+++

Axiom Team est une association qui œuvre pour le développement technique de la Monnaie Libre Ğ1 par l'écosystème logiciel Duniter.

Axiom Team sur le web :

- page HelloAsso [https://www.helloasso.com/associations/axiom-team/](https://www.helloasso.com/associations/axiom-team/)
- groupe sur le forum ML [https://forum.monnaie-libre.fr/g/axiom-team](https://forum.monnaie-libre.fr/g/axiom-team)

Membres :

- [poka](@/contributeurs/poka.md)
- [Hugo](@/contributeurs/HugoTrentesaux.md)
- [tuxmain](@/contributeurs/tuxmain.md)
- [1000i100](@/contributeurs/1000i100.md)
- [elois](@/contributeurs/elois.md)
- [Manutopik](@/contributeurs/manutopik.md)
- [Matograine](@/contributeurs/matograine.md)
- Pi
- Attilax
- Paulart
- Scanlegentil
- Syoul