+++
title = "Article démo"
date = 2000-01-01
draft = true

[taxonomies]
authors = ["HugoTrentesaux",]
tags = ["demo",]

[extra]
thumbnail = "/img/yunohost.png"
+++

# Article de démonstration

Cet article montre les fonctionnalités des shortcode. Il peut être généré avec `zola serve --drafts`.

### Display de notes possible

{% note(type="ok", display="block", markdown=true) %}
Note "ok" *échantillon*. De display "block"  
Avec du contenu en **markdown**.
{% end %}

{% note(type="ok", display="both") %}
Note "ok" de display "both"
{% end %}

{% note(type="ok", display="icon") %}
Note "ok" display "icon"
{% end %}

### Type de notes possible

{% note(type="default") %}
Note "default"
{% end %}

{% note(type="neutral") %}
Note "neutral"
{% end %}

{% note(type="ok") %}
Note "ok"
{% end %}

{% note(type="warning") %}
Note "warning"
{% end %}

{% note(type="error") %}
Note "error"
{% end %}

{% note(type="info") %}
Note "info"
{% end %}

### Taille de note possible

{% note(type="info", markdown="true" size="small") %}
Note "small"  
Avec deux lignes
{% end %}

{% note(type="info", markdown="true" size="medium") %}
Note "medium"  
Avec  
Trois lignes
{% end %}

{% note(type="info", markdown="true" size="large") %}
Note "large"  
Avec  
Cinq  
Lignes  
?
{% end %}