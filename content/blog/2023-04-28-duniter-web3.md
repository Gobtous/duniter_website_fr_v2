+++
title = "Duniter et le web3"
date = 2023-04-28
description = "Le Web3 entend démocratiser l'utilisation d'architectures décentralisées et doter le monde en ligne d'un modèle de gouvernance démocratique. Voici ce que Duniter a à y apporter."

[extra]
thumbnail = "/blog/web3.svg"

[taxonomies]
authors = ["HugoTrentesaux", "Robin"]
tags = ["identité numérique", "toile de confiance"]

[extra.translations]
en = "blog/duniter-web3/"
+++

<br>
<br>

{% note(type="warning") %}
Cet article est un article d'opinion. Son contenu doit être compris comme le point de vue de l'auteur. L'article original est en anglais.
{% end %}


# Duniter et Web3
Le [Web3](https://fr.wikipedia.org/wiki/Web3) est le nom donné à une vision idéale du web où la blockchain permet une gouvernance décentralisée et une économie fondée sur le *token* ou "jeton cryptographique" (au lieu du modèle actuel reposant en grande partie sur la publicité). Mais actuellement, les initiatives Web3 semblent limitées à une élite férue de technologie déjà bien intégrée dans le monde en ligne et ne contribuent pas à réduire la fracture numérique. Elles n'intègrent pas suffisamment la classe ouvrière et les personnes technophobes pour atteindre leurs ambitions démocratiques.

Le **logiciel Duniter** répond à ce problème en implémentant la <abbr title="Théorie Relative de la Monnaie">TRM</abbr>, une théorie monétaire solide qui prouve que la seule forme de création monétaire égalitaire est un **Dividende Universel** (DU) (à ne pas confondre avec un revenu universel). Sa **toile de confiance** sert de base à une identité numérique décentralisée, permettant l'attribution du DU.

Une expérience grandeur nature nommée **monnaie Ğ1** et démarrée en 2017 confirme que le DU est un moyen valable de lutter contre les inégalités sociales et de fournir une véritable unité de mesure de la valeur.  À ce stade, la toile de confiance de la Ğ1 compte 8145 individus avec une grande diversité sociale. Elle se prépare à passer à l'échelle grâce à la [réécriture de Duniter](@/blog/2022-01-29-duniter-substrate.md) dans le *framework* blockchain Substrate, permettant de bénéficier de sa scalabilité et de son écosystème de gouvernance dit *on-chain*.


<span class="center-content"><img src="/blog/web3.svg" alt="duniter avec le logo web3" style="max-width: 200px;"/></span>

> Duniter et web3. Credits [@imppao](https://www.youtube.com/@imppaofree)

## Monnaie Libre
La Ğ1 est une monnaie libre (voir philosophie du [*logiciel libre*](https://www.gnu.org/philosophy/free-sw.fr.html)) au sens de la TRM : une monnaie dans laquelle la création monétaire n'introduit pas d'inégalités. La TRM prouve qu'une telle monnaie doit être émise sous la forme d'un Dividende Universel (DU) distribué périodiquement aux utilisateurs en proportion de la masse monétaire totale. En conséquence, toute monnaie n'implémentant pas le DU introduit des inégalités. Par exemple, le bitcoin favorise ceux qui sont arrivés tôt et ceux qui possèdent des machines puissantes, l'ethereum profite aux premiers arrivés et, depuis 2022, aux stakers (ceux qui possèdent déjà beaucoup de *tokens*), les monnaies fiduciaires profitent aux [*wasp*](https://fr.wikipedia.org/wiki/White_Anglo-Saxon_Protestant), etc.

Le taux de croissance d'une monnaie libre peut être choisi à sa création, mais il est fixé une fois pour toutes (à démographie constante). La Ğ1 cible une population ayant une espérance de vie de ~80 ans et a fait le choix d'un taux de croissance annuel de 10 % afin que la masse monétaire totale provienne à parts égales de personnes vivantes et de personnes décédées ([ref](https://www.creationmonetaire.info/2019/08/c-fvx.html)) dans l'hypothèse d'une démographie constante.


## Toile de confiance (WoT)
Bien que la cryptographie garantisse l'authenticité d'un document grâce à la [signature numérique](https://fr.wikipedia.org/wiki/Signature_num%C3%A9rique), elle n'établit pas le lien entre l'identité numérique et l'individu. Ce problème, connu sous le nom de preuve d'humanité ([proof of personhood](https://en.wikipedia.org/wiki/Proof_of_personhood) en anglais), est réputé difficile.
L’approche par [toile de confiance](https://fr.wikipedia.org/wiki/Toile_de_confiance) utilisée dans PGP pour tenter de résoudre ce problème (c'est-à-dire l'établissement d'un lien entre une clé publique et son propriétaire) semble désormais à l'arrêt ([ref](https://inversegravity.net/2019/web-of-trust-dead/) en anglais). Un séminaire intitulé *Rebooting the Web of Trust* ([weboftrust.info](https://www.weboftrust.info/)) y travaille depuis 2015 par le biais d'événements annuels <abbr title="Rebooting the Web Of Trust">RWOT</abbr>, sans qu'aucune mise en œuvre concrète n'ait encore eu lieu.

La toile de confiance de Duniter, grâce à son astucieux mélange de règles temporelles et statiques ([en savoir plus](2018-04-15-la-toile-de-confiance-en-detail.md)), a permis à la communauté Ğ1 de se développer de manière régulière et soutenue. Sa croissance relativement lente s'est révélée bénéfique à la dimension "confiance" de la toile de *confiance*. Les membres <a href="https://carte.monnaie-libre.fr/?members"> sont bien répartis sur le territoire français</a>, aussi bien dans les zones urbaines que rurales, y compris dans les territoires d'outre-mer. La communauté Ğ1 est impliquée dans la gouvernance de Duniter à travers de nombreuses réunions <abbr title="In Real Life">IRL</abbr> hors chaîne. 

<a href="https://youtu.be/Hj3GpaEYLwA" target="_blank" class="center-content"><img src="/blog/wot_2022-11-22.png" alt="web of trust" style="max-width: 400px;"/></a>

> capture de la toile de confiance au 2022-11-22: 6944 membres, 61737 certifications. Animation disponible sur [https://youtu.be/Hj3GpaEYLwA](https://youtu.be/Hj3GpaEYLwA)

## Consensus blockchain par preuve d’identité
Les algorithmes de consensus de la blockchain peuvent être sans [permission ou avec permission](https://en.wikipedia.org/wiki/Consensus_(computer_science)#Permissioned_versus_permissionless_consensus).
Les algorithmes sans permission connus, comme la *preuve de travail* de Bitcoin, sont lents et gourmands en énergie. C'est pourquoi de nombreuses blockchains se sont orientées vers des algorithmes avec permission, comme la *preuve d'enjeu* ou la *preuve d'autorité*. Alors que Duniter v1 utilisait une preuve mixte ([en savoir plus](@/blog/2017-05-02-preuve-de-travail.md)), Duniter v2 est maintenant prêt à utiliser le mécanisme de preuve d'identité entièrement décentralisé fourni par sa toile de confiance en combinaison avec l'algorithme hybride <a href="https://research.web3.foundation/en/latest/polkadot/block-production/Babe.html"><abbr title="Blind Assignment for Blockchain Extension">BABE</abbr></a>/<a href="https://research.web3.foundation/en/latest/polkadot/finality.html"><abbr title="Greedy Heaviest-Observed Sub-Tree">G</abbr><abbr title="GHOST-based Recursive ANcestor Deriving Prefix Agreement">RANDPA</abbr></a>. Alors que la toile de confiance principale est accessible à tout le monde, un sous-ensemble de celle-ci appellée "toile de confiance forgeron", avec des exigences de sécurité élevées, permettra de participer à l'écriture des blocs. Il s'agira alors du premier consensus de blockchain utilisant une couche de permission entièrement décentralisée avec [preuve d'identité](https://en.wikipedia.org/wiki/Proof_of_identity_(blockchain_consensus)) (PoID) pour BABE. C'est un autre moyen de se rapprocher d'une véritable démocratie sur blockchain (le prochain moyen le plus proche étant, à mon avis, la [NPoS](https://wiki.polkadot.network/docs/learn-consensus#nominated-proof-of-stake)).

## Conception communautaire "orientée outil"
Alors que de nombreux projets Web3 appliquent une démarche de conception "orientée produit", l'écosystème logiciel Duniter est "orienté outil". Il n'essaie pas de créer des besoins avec un marketing agressif, mais répond aux différents besoins des utilisateurs. Il suffit de regarder la galaxie des logiciels libres [connectés à la blockchain de Duniter](@/logiciels/_index.md) pour se faire une idée de la diversité d'utilisation de cette monnaie par rapport à l'aspect principalement financier des *crypto-tokens*.

## Futur de Duniter
Jusqu'à présent, Duniter était principalement confiné aux utilisateurs francophones, mais il s'ouvre au monde extérieur en traduisant sa documentation en anglais et en espagnol et en [réécrivant son cœur](@/blog/2022-01-29-duniter-substrate.md) dans le *framework* Substrate. Cela facilitera l'interopérabilité avec d'autres blockchains et l'extension de son réseau de confiance, l'un de ses principaux atouts.

Si vous souhaitez en savoir plus sur Duniter ou y contribuer, n'hésitez pas à nous contacter sur notre forum technique Discourse.

<span class="center-content">
<a href="https://forum.duniter.org/" class="w3-round w3-button w3-blue"><img src="/img/duniter_forum.png"/> Écrivez-nous !</a>
</span>
