+++
title = "À propos"
date = 2017-03-27
updated = 2020-07-26
weight = 6

[taxonomies]
authors = ["cgeek",]
category = ["Meta",]

[extra.translations]
en = "wiki/about/"
+++

# À propos

## Contenus

L'intégralité de ce site est sous licence [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) sauf mention contraire explicite.

## Code source

Ce site a été réalisé en [Zola](https://www.getzola.org/) avec [w3.css](https://www.w3schools.com/w3css/) et [Forkawesome](https://forkaweso.me/Fork-Awesome/).

Le code source du site est par ailleurs intégralement disponible sur le dépôt [https://git.duniter.org/websites/duniter_website_fr_v2/](https://git.duniter.org/websites/duniter_website_fr_v2/) avec les instructions permettant de le reproduire.

## Images utilisées sur ce site

Les logos officiels des logiciels sont disponibles sur ce dépôt : [https://git.duniter.org/communication/g1](https://git.duniter.org/communication/g1).  
Certains logos ou images sont la production d'autres personnes, vous trouverez leur origine sur la page [Crédits](@/wiki/about/credits.md).

## Qui sommes-nous ?

Plus de détails sur nous dans la page [Qui sommes-nous ?](@/wiki/about/qui-sommes-nous.md)

## Nous contacter

Pour nous contacter, rendez-vous sur [la page de contact](@/wiki/about/contact.md)

## Nous financer

Barre de financement en ğ1 pour le mois courant :

<iframe frameborder="0" name="iframe" src="https://txmn.tk/g1/barre-de-financement/iframe.php?pubkey=78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8&target=793&unit=relative&title=Duniter Project&display_pubkey=true&display_qrcode=true&display_graph=true" width="100%" height="1100px">
