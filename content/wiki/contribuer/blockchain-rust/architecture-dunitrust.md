+++
aliases = ["architecture-dunitrust",]
date = 2018-03-29
weight = 10
title = "Architecture de Dunitrust"

[taxonomies]
authors = ["elois",]
+++

# Architecture de Dunitrust

Dunitrust est composé de 3 types de crates : 

* Les bibliothèques  
* Les plugins  
* la crate duniter-core

## Arbre des dépendances

![uml](/PELICAN/uml/d01b3686.png)
