+++
aliases = ["blockchain-rust",]
date = 2018-05-08
weight = 9
title = "[archive] Contribuer à Dunitrust"

[taxonomies]
authors = ["elois",]
+++

{% note(type="warning") %}
Le projet Dunitrust a été remplacé par le projet Duniter v2s. Ce guide de contribution n'est plus d'actualité.
{% end %}

# Contribuer à Dunitrust

Pages concernant spécifiquement l'implémentation Rust de Duniter nommée Dunitrust

## Développement

* [Installer son environnement Rust](@/wiki/contribuer/blockchain-rust/installer-son-environnement-rust.md)
* [Architecture de Dunitrust](@/wiki/contribuer/blockchain-rust/architecture-dunitrust.md)

## Installation

  * [Cross-Compiler Dunirust pour arm](@/wiki/contribuer/blockchain-rust/cross-compilation-pour-arm.md)
