+++
title = "L’écosystème logiciel de Duniter"
date = 2018-05-15
aliases = ["fr/ecosysteme-logiciel-duniter",]

[extra]
thumbnail = "/PELICAN/images/logos/duniter.png"

[taxonomies]
authors = ["inso",]
tags = ["gitlab",]
+++

# L’écosystème logiciel de Duniter

Nous allons ici vous présenter l'écosystème logiciel de Duniter.

Outre les logiciels historiques, vous découvrirez aussi des projets moins connus mais tout aussi fondamentaux pour l'avenir de Duniter.

[TOC]

## Topologie du réseau

Le réseau Duniter est un réseau pair à pair (P2P). Techniquement, ce type de réseau signifie qu'il n'y a pas de serveur central. Ceux-ci sont répartis, n'importe qui peut démarrer un nouveau nœud et se connecter au réseau. Cependant, à la différence de bitcoin, il n'existe pas de nœud dit "wallet" dans Duniter. Deux objectifs distincts sont définis actuellement entre les nœuds serveurs et les nœuds clients :

  - Les nœuds serveurs ont pour objectif de faire avancer la chaîne de blocs afin de valider les transactions, faire évoluer la toile de confiance.
  - Les nœuds clients permettent aux utilisateurs du réseau de se connecter aux nœuds serveurs afin de vérifier l'état des soldes, des certifications, etc.
      
Il y a donc deux couches dans le réseau Duniter : les serveurs, qui se synchronisent entre eux via la blockchain, et les clients, situés en périphérie, et qui se connecte aux nœuds serveurs.

L'exécution de nœuds serveur est anonyme. Cependant, sans associer le nœud à une identité, celui-ci ne peut pas calculer de bloc : il se comporte en simple miroir de la chaîne de blocs. Une fois associé à une identité membre, il peut participer au calcul de blocs sur la chaîne.


## Nœuds serveurs

Il n'existe dans Duniter qu'une seule implémentation des nœuds serveurs à l'heure actuelle : duniter-ts. Il est écrit en [TypeScript](https://www.typescriptlang.org/), un langage orienté-objet et typé, qui est transpilé en JavaScript. Ce langage offre l'avantage de la robustesse des langages typés tout en offrant l'agilité et la vitesse de développement de JavaScript.

Duniter-ts est un nœud serveur qui est configurable en ligne de commande. Il est capable d'automatiquement se connecter et se synchroniser au réseau, y compris derrière un NAT. Pour en simplifier l'utilisation, un plugin, Duniter-UI a été développé. Il permet d'avoir une interface d'administration pour ce logiciel.


<span class="center-content"><image src="/PELICAN/images/duniter-ui.png" width="400"/></span>

Duniter gère ainsi différents modules. Le plus connu étant [Duniter-Currency-monit](https://g1-monit.monnaielibreoccitanie.org/). Un outil permettant d'observer l'évolution de la monnaie, que ce soit la toile de confiance, les adhésions, la masse monétaire, etc.


Le protocole réseau des nœuds serveurs permet aux nœuds de s'auto-déclarer via un message signé contenant leurs différents points d’accès (_endpoints_). Un nœud peut héberger plusieurs types d’endpoints. Par exemple :

  - **BMA** : API historique de Duniter, aujourd'hui seulement utilisée par les clients. Sur un modèle REST, elle permet aux clients d'obtenir des données sur la chaîne de blocs (statut d'adhésion des membres, sources de monnaie disponible, etc)
  - **WS2P** : API née avec la version 1.6 de duniter-ts. Elle gère la communication P2P entre les nœuds serveurs et clients. Elle offre la fonctionnalité d'outrepasser les problèmes de NAT en offrant deux modes de fonctionnement : public et privé. Un nœud public a des slots de connexions disponibles pour tous les autres nœuds. Les nœuds privés se connectent sur les nœuds publics pour obtenir leurs données du réseau.
  - **ES_API** : API développée pour le client Césium permettant d'héberger un nœud Elastic Search en parallèle d'un nœud Duniter. Ce nœud va se synchroniser sur le nœud Duniter, et permettre aux clients de faire une requête des informations très avancées et détaillées en un temps très court. Ces endpoints sont utilisés lorsque le module *Césium+* est activé sur Césium.


## Logiciels clients

La communauté de développeur a réalisé à l'heure actuelle trois types de logiciels *wallets* pour Duniter : 

Un client CLI, [Silkaj](https://git.duniter.org/clients/python/silkaj). Réalisé en Python 3, il permet d'automatiser facilement des tâches sur le réseau. Il offre une liste de fonctionnalité assez complète :  

 - Affichage d'information sur la monnaie, l'état du réseau, la difficulté de la preuve de travail individuelle, etc
 - Envoi de monnaie et affichage du solde des comptes
 - État de la toile de confiance (qui est membre, certifications reçues et envoyées, etc)

Silkaj n'utilise pas le consensus du réseau pour vérifier ses réponses. Il requête un nœud, que l'utilisateur peut passer en paramètre des différentes commandes. Silkaj est multiplateforme.

Un client lourd, [Sakia](https://git.duniter.org/clients/python/sakia). Réalisé avec PyQt5 et Python 3, il offre les fonctionnalités de base d'un wallet Duniter, tel que :  

   - Affichage du solde de chacun de ses comptes, envoi de monnaie
   - Gestion de son statut d'adhésion, renouvellement, émission de certifications
   - Recherche dans l'annuaire, étude des identités présentes sur le réseau
   - Observation du réseau, des endpoints disponibles, etc
  Sakia utilise le consensus réseau pour vérifier les réponses des nœuds. Lorsqu'il requête une donnée, il va comparer les réponses de différents nœuds pour s'assurer de leur cohérence. En conséquence, Sakia est plus lent que les autres clients. Sakia est multiplateforme.
  
<span class="center-content"><image src="/PELICAN/images/sakia-ui.png" width="400"/></span>

Un client web, [Cesium](https://git.duniter.org/clients/cesium/cesium). Réalisé avec ionic.js, c'est le client le plus avancé en termes de fonctionnalités utilisateur. Il permet, de base, les fonctions suivantes :

  - Affichage du solde de son compte, envoi de monnaie, etc
  - Affichage du statut WS2P du réseau
  - Recherche dans l'annuaire, étude des identités disponibles sur le réseau

 Cesium offre la possibilité d'activer un plugin nommé Césium+. Ce plugin permet d'activer des fonctionnalités supplémentaires qui n'utilisent pas la blockchain mais le serveur Elastic Search [Duniter4j](https://github.com/duniter/duniter4j). Les fonctionnalités ajoutées par ce plugin sont :  
 
  - Affichage d'un avatar associé aux identités dans la blockchain
  - Ajout de pages permettant aux entreprises, sociétés et associations de faire connaître leur usage de la Ğ1
  - Affichage d'une carte permettant de localiser les membres afin de simplifier les rencontres
  - Intégration d'une messagerie chiffrée avec les clés Duniter
  En tant que client web, Césium est installable sur toutes les plateformes, que ce soit des ordinateurs de bureau ou des ordiphones. 
  
<span class="center-content"><image src="/PELICAN/images/cesium-ui.png" width="400"/></span>


## Places de marchés

Afin de permettre à la monnaie de se développer, deux places de marché ont été développées. Chacune apporte ses propres fonctionnalités : 

- [Ğannonce](https://gannonce.duniter.org/) est un logiciel de place de marché implémenté sous la forme d'un plugin sur un nœud Duniter. Cette place de marché offre une fonctionnalité intéressante pour financer les différentes initiatives : la possibilité de mettre en place un [crowdfunding](https://gannonce.duniter.org/#/announce/576abc59-621f-4187-a76b-9ae6e9777a2e). Pour ce faire, il suffit d'envoyer une transaction avec le commentaire associé permettant de la retrouver dans la chaîne de blocs. Le logiciel met alors automatiquement à jour la jauge de financement.

- [Ğchange](https://www.gchange.fr/#/app/home) est aussi un logiciel de place de marché. Il est implémenté sous la forme d'une interface ionic.js utilisant un serveur Elasticsearch en backend. Comme Césium, il offre des fonctionnalités de création de pages pour les entreprises. Il permet de géolocaliser les différentes annonces. C'est aujourd'hui la place de marché la plus active de la communauté.
    
<span class="center-content"><image src="/PELICAN/images/gchange-ui.png" width="400"/></span>


## Autres projets

D'autres projets existent ou sont en cours de développement. On peut noter parmi ceux existants :

- [Remuniter](https://remuniter.cgeek.fr/), le premier service de financement des calculateurs. Il fonctionne sous la forme d'une caisse commune : n'importe qui peut déposer de la monnaie sur la clé publique associée au service. Il va alors automatiquement verser une petite somme (0,20 Ğ1 par bloc, à l'heure actuelle) aux nœuds qui participent au réseau. Ce service est actuellement complètement centralisé et fonctionne sur la confiance faites en celui qui héberge la redistribution de Ğ1. Il est implémenté sous la forme d'un plugin Duniter.  
- [Wot Wizard](https://wot-wizard.duniter.org/), un outil développé en Component-Pascal permettant de calculer les probabilités futures d’entrés d’une identité dans la toile de confiance. En effet, du fait de la forme chaotique des entrés dans la toile de confiance, il était nécessaire pour les utilisateurs de savoir quand est-ce qu'ils deviendraient, de manière sûre et certaine, membre de la toile.
- [Gsper](https://g1.frama.io/gsper/), destiné à essayer de retrouver son identifiant et mot de passe perdu. Il permet de générer des variations des identifiants et mot de passe qu'on lui donne, et de tester toutes les combinaisons pour voir si l'une d'elle correspond.
- [PaperWallet] (~~broken link~~) un système d'impression de wallet de stockage à froid. Ce système permet d'imprimer un QRCode contenant l'adresse de réception des ğ1 ainsi que la clé privée. Il permet donc de stocker chez soi des ğ1 sur une clé déconnectée du web.

Du côté des prochains logiciels à venir, on ne va pas s'ennuyer :

- [ĞVu](https://git.duniter.org/clients/gvu), un outil de visualisation de la toile de confiance de Duniter. Avec les filtres disponibles dans l'interface, il est possible d'étudier la toile de confiance, sa topologie, etc.
- [Duniter-rs](https://git.duniter.org/nodes/rust/duniter-rs), une implémentation du nœud Duniter en Rust. L'objectif de cette seconde implémentation est de renforcer la sécurité de l'écosystème Duniter. Une faille ou un bug dans la version TypeScript pourrait aujourd'hui être très dommageable pour le réseau. Avec une implémentation en Rust qui fonctionnerait en parallèle, la résilience de la monnaie serait bien meilleure ! Ce logiciel pourra aussi fonctionner sur des configurations plus légères que duniter-ts. Pour le moment, ce nœud n'est pas utilisable pour fonctionner sur le réseau Duniter. Cependant, les travaux avancent rapidement, et les RML11 seront l'occasion pour Éloïs de nous présenter tout le travail réalisé jusqu'à présent !
- [Ğ1Billet](https://git.duniter.org/tools/g1billet), un système complet de génération de billets Ğ1. Ce système repose sur plusieurs mécanismes, dont notamment :
    - Impression d'un visage sur un billet, pour rendre plus difficile les doublons de billets
    - Utilisation d'un système où le billet doit être détruit pour être consommé et transférer les ğ1 sur son compte
    - Implémentation d'un service web qui référence l'utilisation des billets existants, dans l'espace et dans le temps, afin de pouvoir vérifier qu'un billet reçu n'a pas été utilisé en même temps dans une distance lointaine.

<span class="center-content"><image src="/PELICAN/images/g1billet.png" width="400"/></span>
<span class="center-content"><i>Exemple de billet envisagé</i></span>

Enfin, les développeurs s'intéressent aujourd'hui aux idées du projet [FYgg](https://framagit.org/fygg) développé par nanocryk. Certains concepts utilisés dans ce framework de chaîne de blocs permettraient de décentraliser davantage Duniter, de fiabiliser et accélérer les données retournée par le réseau aux clients. En parallèle de ces recherches, les développeurs réfléchissent à l'implémentation d'une [nouvelle API clients](https://git.duniter.org/nodes/common/doc/merge_requests/3), qui s'appuierait sur GraphQL.

Si vous voulez en découvrir plus, nous vous invitons à vous rendre aux [onzième rencontres de la monnaie libre](https://rml11.duniter.org/), à Douarnenez, en Bretagne ! Ce sont quatre jours intenses où les développeurs et informaticiens peuvent échanger sur les différents projets tournant autour de cette monnaie, et où les utilisateurs peuvent découvrir de nouveaux usages !

